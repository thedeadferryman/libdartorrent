//
// Created by Karl Meinkopf on 11.03.2021.
//

#include <dartorrent.h>
#include <iostream>

using namespace std;

int main(int argc, char **argv) {
	auto result = CDEF_EXPORT_TorrentCreator_fromDirectory(
			"../src",
			"../src.torrent",
			"dartorrent/check",
			"",
			[](const char *ff) {
				return true;
			}
	);

	cout << result << endl;
}