//
// Created by Karl Meinkopf on 12.03.2021.
//

#include "dartorrent.h"

#include <string>

#include <libtorrent/file_storage.hpp>
#include <libtorrent/create_torrent.hpp>
#include <filesystem>
#include <fstream>

using String = std::string;

namespace fs = std::filesystem;

CDEF_EXPORT_TorrentCreator_Result
CDEF_EXPORT_TorrentCreator_fromDirectory(
		const char *inputPath,
		const char *outputPath,
		const char *creator,
		const char *comment,
		CDEF_EXPORT_TorrentCreator_FileFilter fileFilter
) {
	lt::file_storage fs;

	auto path = fs::canonical(inputPath);

	if(!fs::is_directory(path)) {
		return INPUT_NOT_DIRECTORY;
	}

	lt::add_files(fs, path.string(), [fileFilter](const String& s) -> bool {
		return fileFilter(s.c_str());
	});

	auto tc = lt::create_torrent(fs, 0, lt::create_torrent::v1_only);

	tc.set_comment(comment);
	tc.set_creator(creator);

	lt::set_piece_hashes(tc, path.parent_path().string());

	auto outStr = std::ofstream(fs::absolute(outputPath), std::ios::binary | std::ios::out);

	outStr.exceptions(std::ofstream::goodbit);

	if(!outStr.is_open()) {
		return OUTPUT_NOT_WRITABLE;
	}

	auto entry = tc.generate();

	lt::bencode(std::ostream_iterator<char>(outStr), entry);

	outStr.close();

	return outStr.good() ? SUCCESS : OUTPUT_NOT_WRITABLE;
}
