//
// Created by Karl Meinkopf on 11.03.2021.
//

#ifndef DARTORRENT_DARTORRENT_H
#define DARTORRENT_DARTORRENT_H

#define TCREATOR(a) static_cast<file_storage*>(a)

#ifdef __cplusplus
extern "C" {
#endif

typedef enum CDEF_EXPORT_TorrentCreator_Result {
	SUCCESS,
	INPUT_NOT_DIRECTORY,
	OUTPUT_NOT_WRITABLE,
} CDEF_EXPORT_TorrentCreator_Result;

typedef bool (*CDEF_EXPORT_TorrentCreator_FileFilter)(const char *);

CDEF_EXPORT_TorrentCreator_Result CDEF_EXPORT_TorrentCreator_fromDirectory(
		const char *inputPath,
		const char *outputPath,
		const char *creator,
		const char *comment,
		CDEF_EXPORT_TorrentCreator_FileFilter fileFilter
);

#ifdef __cplusplus
}
#endif

#endif //DARTORRENT_DARTORRENT_H
